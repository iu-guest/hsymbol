{-# LANGUAGE TemplateHaskell #-}
module HSymbol.Types
  ( Import(..)
  , Tag
  , Storage
  )
where

import Data.ByteString (ByteString)
import Data.Store ()
import Data.Store.TH (makeStore)
import Data.Map (Map)
import Data.Set (Set)

data Import = Import { module_ :: ByteString, as :: Maybe ByteString }
  deriving (Show, Eq, Ord)
type Tag = ByteString

type Storage = Map Tag (Set Import)

$(makeStore ''Import)
