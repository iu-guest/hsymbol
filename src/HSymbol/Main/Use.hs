{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
module HSymbol.Main.Use (main) where

import System.IO (withFile, IOMode(ReadMode))
import Control.Exception (throwIO)
import Data.Text (Text)
import qualified Data.Text.IO as TO
import System.Exit (exitFailure)
import HSymbol.Types (Import(..))
import qualified Data.Map as Map
import qualified Data.Set as Set
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as C8
import Data.Store (decode)
import System.Posix.Env.ByteString (getArgs)
import Data.Maybe (fromMaybe)
import System.Directory (getCurrentDirectory, doesFileExist)
import System.FilePath.Posix ((</>))
import Data.List (groupBy)

usage :: Text
usage = "usage: hsymbol {tag}"

locateDatabase :: IO FilePath
locateDatabase = fmap (++ "/") getCurrentDirectory >>= go
  where
    name = "hsymbol.db"
    go path =
      let candidate     = path </> name
          notFoundError = throwIO.userError $ name <> " not found"
          notSlash a b  = a /= '/' && b /= '/'
          init2         = init . init
      in  doesFileExist candidate >>= \case
            True -> pure candidate
            False | path == "/" -> notFoundError
            False ->
              let segments = groupBy notSlash path
                  parent   = concat (init2 segments)
              in  go parent

main :: IO ()
main = getArgs >>= \case
  [tag] -> do
    db <- locateDatabase
    withFile db ReadMode $
      \handle -> (decode <$> B.hGetContents handle) >>= \case
        Left  e -> print e *> exitFailure
        Right m -> do
          let imports = fromMaybe Set.empty $ Map.lookup tag m
              printImport (Import mod' as') =
                C8.putStrLn $ "-m " <> mod' <> maybe "" (" -q " <>) as'
          foldMap printImport imports
  _     -> TO.putStrLn usage *> exitFailure
